import {Route, BrowserRouter as Router, Routes} from "react-router-dom";
import React from "react";
import Home from "./routes/Home";
import Shop from "./routes/Shop";
import Projects from "./routes/Projects";
import About from "./routes/About";
import Gallery from "./routes/Gallery";

const App = () => {
  return (
      <div>
          <Router basename={process.env.PUBLIC_URL ?? "/"}>
              <Routes>
                  <Route exact path={"/"} element={<Home/>}/>
                  <Route exact path={"/projects"} element={<Projects/>}/>
                  <Route exact path={"/about"} element={<About/>}/>
                  <Route exact path={"/shop"} element={<Shop/>}/>
                  <Route exact path={"/gallery"} element={<Gallery/>}/>
                  <Route path={"*"} element={<p>404</p>}/>
              </Routes>
          </Router>
      </div>
  );
}

export default App;
