import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

const LinkButton = ({title, to}) => {
    return (
        <Link to={to} className={"w-full md:w-content h-content"}>
        <div
            className={"w-full h-content md:w-80 md:px-4 py-4 md:py-8 bg-contain md:bg-cover cursor-pointer text-center bg-center bg-no-repeat"}
            style={{backgroundImage: `url(assets/buttons/teared_paper.png)`}}
        >
            <p className={"font-chantal font-bold text-4xl md:text-6xl"}>{title}</p>
        </div>
        </Link>
    );
};

LinkButton.propTypes = {
    title: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired
};

export default LinkButton;