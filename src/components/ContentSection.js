import React from 'react';

const ContentSection = ({children}) => {
    return (
        <div className={"w-full h-6/10"}>
            <img src={"assets/backgrounds/header.png"} alt={"logo"} className={"w-full"}/>
            <div
                className={"w-full h-full bg-cover bg-center"}
                style={{backgroundImage: `url(assets/backgrounds/content.png)`}}
            >
                {children}
            </div>
            <img src={"assets/backgrounds/footer.png"} alt={"logo"} className={"w-full"}/>
        </div>
    );
};

ContentSection.propTypes = {

};

export default ContentSection;