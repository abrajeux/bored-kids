import React from 'react';
import ContentSection from "../components/ContentSection";
import Header from "./Header";

const Shop = () => {
    return (
        <div
            className={"w-screen h-screen bg-center bg-cover bg-no-repeat"}
            style={{backgroundImage: `url(assets/backgrounds/shop.png)`}}
        >
            <Header title={"Shop"}/>
            <ContentSection>
                <p>Shop</p>
            </ContentSection>

        </div>
    );
};

Shop.propTypes = {

};

export default Shop;