import React from 'react';
import ContentSection from "../components/ContentSection";
import Header from "./Header";

const About = () => {
    return (
        <div
            className={"w-screen h-screen bg-center bg-cover bg-no-repeat"}
            style={{backgroundImage: `url(assets/backgrounds/about.png)`}}
        >
            <Header title={"About"}/>
            <ContentSection>
                <p>About</p>
            </ContentSection>

        </div>
    );
};

About.propTypes = {

};

export default About;