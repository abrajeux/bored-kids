import React from 'react';
import ContentSection from "../components/ContentSection";
import Header from "./Header";

const Projects = () => {
    return (
        <div
            className={"w-screen h-screen bg-center bg-cover bg-no-repeat"}
            style={{backgroundImage: `url(assets/backgrounds/projects.png)`}}
        >
            <Header title={"Projects"}/>
            <ContentSection>
                <div className={"py-16 h-full"}>
                    <img src={"assets/logos/wip.png"} alt={"wip"} className={"w-1/4 mx-auto"}/>
                </div>
            </ContentSection>

        </div>
    );
};

Projects.propTypes = {

};

export default Projects;