import React from 'react';
import LinkButton from "../components/LinkButton";
import {Link} from "react-router-dom";

const Header = ({title}) => {
    return (
        <div className={"w-full flex justify-center items-center relative -mb-12"}>
            <Link to={process.env.HOMEPAGE ?? "/"} className={"absolute left-0"}>
                <img src={"assets/buttons/home.png"} alt={"home_button"} className={"w-40"}/>
            </Link>
            <LinkButton title={title} to={"#"}/>
        </div>
    );
};

Header.propTypes = {

};

export default Header;