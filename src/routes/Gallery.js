import React from 'react';
import ContentSection from "../components/ContentSection";
import Header from "./Header";

const Gallery = () => {
    return (
        <div
            className={"w-screen h-screen bg-cover bg-no-repeat"}
            style={{backgroundImage: `url(assets/backgrounds/gallery.png)`}}
        >
            <Header title={"Gallery"}/>
            <ContentSection>
                <p>Gallery</p>
            </ContentSection>

        </div>
    );
};

Gallery.propTypes = {

};

export default Gallery;