import React from 'react';
import LinkButton from "../components/LinkButton";


const Home = () => {
    return (
        <div
            className={"w-screen h-screen overflow-hidden bg-cover bg-center bg-no-repeat py-8 md:px-8"}
            style={{backgroundImage: `url(assets/backgrounds/home.png)`}}
        >
            <div className={"w-full h-full flex flex-col md:justify-between items-center space-y-6 md:space-y-0"}>
                <img src={"assets/logos/loop-vertical.gif"} alt={"logo"} className={"block md:hidden w-3/4 mb-12"}/>
                <div className={"w-full flex flex-col md:flex-row justify-between items-center space-y-6 md:space-y-0"}>
                    <LinkButton to={"/projects"} background={null} title={"Projects"}/>
                    <LinkButton to={"/about"} background={null} title={"About"}/>
                </div>
                <div className={"w-full justify-center hidden md:flex"}>
                    <img src={"assets/logos/loop.gif"} className={"w-1/2"} alt={'logo_loop'}/>
                </div>
                <div className={"w-full flex flex-col md:flex-row justify-between items-center space-y-6 md:space-y-0"}>
                    <LinkButton to={"/shop"} background={null} title={"Shop"}/>
                    <LinkButton to={"/gallery"} background={null} title={"Gallery"}/>
                </div>
            </div>
        </div>
    );
};

export default Home;