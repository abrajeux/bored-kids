#!/bin/bash

DEPLOY_SERVER=$DEPLOY_SERVER
SERVER_FOLDER="coinwave.info"

# Building React output
yarn install
yarn run build

echo "Deploying to ${DEPLOY_SERVER}"
#scp -r build/* debian@${DEPLOY_SERVER}:/home/debian/test
scp -r build/* debian@${DEPLOY_SERVER}:/var/www/html/${SERVER_FOLDER}

echo "Finished copying the build files"