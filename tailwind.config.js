/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      width: {
        'content': "fit-content",
      },
      height: {
        'content': "fit-content",
        '9/10': "90%",
        '8/10': "80%",
        '7/10': "70%",
        '6/10': "60%",
        '1/2': "50%",
        '4/10': "40%",
        '3/10': "30%",
        '1/5': "20%",
        '1/10': "10%",
      },
      fontFamily: {
        'chantal': ['Chantal', 'sans-serif'],
      }
    },
  },
  plugins: [],
}
